/*---------------------------------------------
Template name :  Ideabuz
Version       :  1.0
Author        :  ThemeLooks
Author url    :  http://themelooks.com

[Table of Content]

    01: Home Banner Slider
    02: Video Popup
    03: Contact Info Svg 
    04: Testimonial Slider
    05: Brand Logo Slider
    06: Project Isotope
    07: Counter up
    08: Team Svg
    09: Blog Slider
    10: Map
----------------------------------------------*/

( function( $ ) {
	"use strict";
	
	/*===================
    01: Home Banner Slider
    =====================*/
	var IdeabuzSlider = function( $scope, $ ) {
		/*==================================
	    05: Check Data
	    ====================================*/
	    var checkData = function (data, value) {
	        return typeof data === 'undefined' ? value : data;
	    };
		/*==================================
		06: Owl Carousel 
		====================================*/
		var $owlCarousel = $scope.find('.owl-carousel');
		$owlCarousel.each(function () {
			var $t = $(this);
			var $leftimage = imgurl.sliderleftimg;
			var $rightimage = imgurl.sliderrightimg;
			$t.owlCarousel({
				items: checkData($t.data('owl-items'), 1),
				margin: checkData($t.data('owl-margin'), 0),
				loop: checkData($t.data('owl-loop'), true),
				smartSpeed: 450,
				autoplay: checkData($t.data('owl-autoplay'), true),
				autoplayTimeout: checkData($t.data('owl-speed'), 15000),
				center: checkData($t.data('owl-center'), false),
				animateIn: checkData($t.data('owl-animate-in'), false),
				animateOut: checkData($t.data('owl-animate-out'), false),
				nav: checkData($t.data('owl-nav'), false),
				navText: ['<img src="' + $leftimage +'" class="svg">', '<img src="' + $rightimage +'" class="svg">'],
				dots: checkData($t.data('owl-dots'), false),
				responsive: checkData($t.data('owl-responsive'), {})
			});
		});

	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/singlebannerslider.default', IdeabuzSlider );
	} );
	
	
	/*========================
	02: Video Popup
	==========================*/
	var IdeabuzVideoarea = function( $scope, $ ) {
	
	    var $popUpVideo = $('.popup-video');
	    if ($popUpVideo.length) {
	        $popUpVideo.magnificPopup({
	            type: 'iframe'
	        });
	    };
		/*==================================
		09: Changing svg color 
		====================================*/
		var $svg_video = $scope.find('.popup-video img.svg');
		$svg_video.each(function () {
			var $img = $(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			$.get(imgURL, function (data) {
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass + ' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, else we gonna set it if we can.
				if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml');
		});
		
	};
	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/videoarea.default', IdeabuzVideoarea );
	} );
	
	/*========================
	03: Contact Info Svg 
	==========================*/
	var IdeabuzContactInfo = function( $scope, $ ) {
		/*==================================
		09: Changing svg color 
		====================================*/
		var $svg_contact_info = $scope.find('.contact-info .image img');
		$svg_contact_info.each(function () {
			var $img = $(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			$.get(imgURL, function (data) {
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass + ' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, else we gonna set it if we can.
				if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml');
		});
		
	};
	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/contactinfo.default', IdeabuzContactInfo );
	} );
	
	/*========================
	04: Testimonial Slider
	==========================*/
	var IdeabuzTestimonial = function( $scope, $ ) {
		/*==================================
		05: Check Data
		====================================*/
		var checkData = function (data, value) {
			return typeof data === 'undefined' ? value : data;
		};
		/*==================================
		06: Owl Carousel 
		====================================*/
		var $owlCarousel = $scope.find('.owl-carousel');
		$owlCarousel.each(function () {
			var $t = $(this);
			var $leftimage = imgurl.sliderleftimg;
			var $rightimage = imgurl.sliderrightimg;
			$t.owlCarousel({
				items: checkData($t.data('owl-items'), 1),
				margin: checkData($t.data('owl-margin'), 0),
				loop: checkData($t.data('owl-loop'), true),
				smartSpeed: 450,
				autoplay: checkData($t.data('owl-autoplay'), true),
				autoplayTimeout: checkData($t.data('owl-speed'), 15000),
				center: checkData($t.data('owl-center'), false),
				animateIn: checkData($t.data('owl-animate-in'), false),
				animateOut: checkData($t.data('owl-animate-out'), false),
				nav: checkData($t.data('owl-nav'), false),
				navText: ['<img src="' + $leftimage +'" class="svg">', '<img src="' + $rightimage +'" class="svg">'],
				dots: checkData($t.data('owl-dots'), false),
				responsive: checkData($t.data('owl-responsive'), {})
			});
		});

	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/testimonialslider.default', IdeabuzTestimonial );
	} );
	
	/*========================
	05: Brand Logo Slider
	==========================*/
	var IdeabuzBrandLogo = function( $scope, $ ) {
		/*==================================
		05: Check Data
		====================================*/
		var checkData = function (data, value) {
			return typeof data === 'undefined' ? value : data;
		};
		/*==================================
		06: Owl Carousel 
		====================================*/
		var $owlCarousel = $scope.find('.brand-logo.owl-carousel');
		$owlCarousel.each(function () {
			var $t = $(this);
			var $leftimage = imgurl.sliderleftimg;
			var $rightimage = imgurl.sliderrightimg;
			$t.owlCarousel({
				items: checkData($t.data('owl-items'), 1),
				margin: checkData($t.data('owl-margin'), 0),
				loop: checkData($t.data('owl-loop'), true),
				smartSpeed: 450,
				autoplay: checkData($t.data('owl-autoplay'), true),
				autoplayTimeout: checkData($t.data('owl-speed'), 15000),
				center: checkData($t.data('owl-center'), false),
				animateIn: checkData($t.data('owl-animate-in'), false),
				animateOut: checkData($t.data('owl-animate-out'), false),
				nav: checkData($t.data('owl-nav'), false),
				navText: ['<img src="' + $leftimage +'" class="svg">', '<img src="' + $rightimage +'" class="svg">'],
				dots: checkData($t.data('owl-dots'), false),
				responsive: checkData($t.data('owl-responsive'), {})
			});
		});

	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/brandlogo.default', IdeabuzBrandLogo );
	} );
	
	/*========================
	06: Project Isotope
	==========================*/
	var IdeabuzProject = function( $scope, $ ) {
		/*==================================
	    12: Isotope
	    ==================================== */
		var $isotope = $('.project-items');
        $isotope.isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            animationOptions: {
                duration: 750,
                easing: "linear",
                queue: false
            },
            masonry: {
                columnWidth: '.grid-item'
            }
        });
		
		// layout Isotope after each image loads
		$isotope.imagesLoaded().progress( function() {
		  $isotope.isotope('layout');
		});
    
        $('.project_filter li').on('click', function () {
            $(this).addClass('active').siblings().removeClass('active');
            var filterValue = $(this).attr('data-filter');
            $('.grid').isotope({
                filter: filterValue
            });
        });
		var $portfolioItems = $scope.find('.project-items');
		let $post_limit = $(".project-items").data("post-limit");
		let $current_page = $(".project-items").data("current-page");
		let $max_page = $(".project-items").data("max-page");
		
		var $ajax_call = $scope.find( '.ajax_call' );
	
	    $ajax_call.click(function(){
		var button = $(this),
			data = {
				'action': 'ideabuz_loadmore',
				'post_limit': $post_limit,
				'page' : $current_page
			};
		$.ajax({ // you can also use $.post here
			url : ideabuz_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Load More...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					let content = $(data); 
					$portfolioItems
						.append(content)
						.isotope('appended', content )
						.isotope("reloadItems")
						.isotope({
							masonry: {
								columnWidth: '.grid-item'
							}
						});
				
						$portfolioItems.imagesLoaded().progress(function(){
							$portfolioItems.isotope('layout');
						});
						
					button.text('Load More');
					$current_page++;

					if ( $current_page == $max_page ){
						button.text('No More Posts').addClass('disabled'); // if last page, remove the button
					}
					// you can also fire the "post-load" event here if you use a plugin that requires it
					// $( document.body ).trigger( 'post-load' );
				} else {
					button.text('No More Posts').addClass('disabled'); // if no data, remove the button as well
				}
			}
		});
		});
	};
	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/projectwidget.default', IdeabuzProject );
	} );
	
	/*========================
	07: Counter up
	==========================*/
	var IdeabuzCounter = function( $scope, $ ) {
		/*==================================
		07: Counter Up
		====================================*/
		var $counterup = $scope.find('.count span');
		$counterup.counterUp({
			delay: 30,
			time: 2000
		});

	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/counterup.default', IdeabuzCounter );
	} );
	
	/*========================
	08: Team Svg
	==========================*/
	var IdeabuzTeam = function( $scope, $ ) {
		/*==================================
		09: Changing svg color 
		====================================*/
		var $svg_team = $scope.find('.single-team-member img.svg');
		$svg_team.each(function () {
			var $img = $(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			$.get(imgURL, function (data) {
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass + ' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, else we gonna set it if we can.
				if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml');
		});
	}
	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/teammember.default', IdeabuzTeam );
	} );
	
	/*========================
	09: Blog Slider
	==========================*/
	var IdeabuzBlogSlider = function( $scope, $ ) {
		/*==================================
		09: Changing svg color 
		====================================*/
		var $svg_blog = $scope.find('.blog-button img.svg');
		$svg_blog.each(function () {
			var $img = $(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			$.get(imgURL, function (data) {
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass + ' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, else we gonna set it if we can.
				if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml');
		});
		/*==================================
		05: Check Data
		====================================*/
		var checkData = function (data, value) {
			return typeof data === 'undefined' ? value : data;
		};
		/*==================================
		06: Owl Carousel 
		====================================*/
		var $owlCarousel = $scope.find('.blog-slider.owl-carousel');
		$owlCarousel.each(function () {
			var $t = $(this);
			var $leftimage = imgurl.sliderleftimg;
			var $rightimage = imgurl.sliderrightimg;
			$t.owlCarousel({
				items: checkData($t.data('owl-items'), 1),
				margin: checkData($t.data('owl-margin'), 0),
				loop: checkData($t.data('owl-loop'), true),
				smartSpeed: 450,
				autoplay: checkData($t.data('owl-autoplay'), true),
				autoplayTimeout: checkData($t.data('owl-speed'), 15000),
				center: checkData($t.data('owl-center'), false),
				animateIn: checkData($t.data('owl-animate-in'), false),
				animateOut: checkData($t.data('owl-animate-out'), false),
				nav: checkData($t.data('owl-nav'), false),
				navText: ['<img src="' + $leftimage +'" class="svg">', '<img src="' + $rightimage +'" class="svg">'],
				dots: checkData($t.data('owl-dots'), false),
				responsive: checkData($t.data('owl-responsive'), {})
			});
		});

	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/blogslider.default', IdeabuzBlogSlider );
	} );
	
	/*========================
	10: Map
	==========================*/
	var IdeabuzMap = function( $scope, $ ) {
		/*==================================
	    10: Google map 
	    ====================================*/
	    var $map = $('[data-trigger="map"]'),
	        $mapOps;
		if( ideabuz_map_api.maploaded == '1' ) {
		    if ($map.length) {
		        // Map Options
		        $mapOps = $map.data('map-options');
		
		        // Map Initialization
		        window.initMap = function () {
		            $map.css('min-height', '600px');
		            $map.each(function () {
		                var $t = $(this), map, lat, lng, zoom;
		
		                $mapOps = $t.data('map-options');
		                lat = parseFloat($mapOps.latitude, 10);
		                lng = parseFloat($mapOps.longitude, 10);
		                zoom = parseFloat($mapOps.zoom, 10);
		
		                map = new google.maps.Map($t[0], {
		                    center: { lat: lat, lng: lng },
		                    zoom: zoom,
		                    scrollwheel: false,
		                    disableDefaultUI: true,
		                    zoomControl: true,
		                    styles: 
		                        [
		                            {
		                                "featureType": "all",
		                                "elementType": "labels.text.fill",
		                                "stylers": [
		                                    {
		                                        "saturation": 36
		                                    },
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 40
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "all",
		                                "elementType": "labels.text.stroke",
		                                "stylers": [
		                                    {
		                                        "visibility": "on"
		                                    },
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 16
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "all",
		                                "elementType": "labels.icon",
		                                "stylers": [
		                                    {
		                                        "visibility": "off"
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "administrative",
		                                "elementType": "geometry.fill",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 20
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "administrative",
		                                "elementType": "geometry.stroke",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 17
		                                    },
		                                    {
		                                        "weight": 1.2
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "landscape",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 20
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "poi",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 21
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "road.highway",
		                                "elementType": "geometry.fill",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 17
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "road.highway",
		                                "elementType": "geometry.stroke",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 29
		                                    },
		                                    {
		                                        "weight": 0.2
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "road.arterial",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 18
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "road.local",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 16
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "transit",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 19
		                                    }
		                                ]
		                            },
		                            {
		                                "featureType": "water",
		                                "elementType": "geometry",
		                                "stylers": [
		                                    {
		                                        "color": "#000000"
		                                    },
		                                    {
		                                        "lightness": 17
		                                    }
		                                ]
		                            }
		                        ]
		                });
		
		                map = new google.maps.Marker({
		                    position: { lat: lat, lng: lng },
		                    map: map,
		                    animation: google.maps.Animation.DROP,
		                    draggable: false,
		                    icon: ideabuz_map_api.mapmarker
		                });
		
		            });
		        };
		        initMap();
		    };
		}
	};

	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/mapwidget.default', IdeabuzMap );
	} );

} )( jQuery );
