/*

[Main Script]

Project: ideabuz
Version: 1.1
Author : themelooks.com

*/

(function ($) {
    'use strict';
    
    
	$( function(){
			
		/* ------------------------------------------------------------------------- *
        * Mail Chimp ajax
        * ------------------------------------------------------------------------- */
				
        var $subscribeForm = $('#subscribe_submit');
     
        $subscribeForm.on('submit', function () {

			var $t = $(this),
			email = $('#sectsubscribe_email').val();
			$.ajax({
				
				type: 'POST',
				url: subscribeajax.action_url,
				data: {
				  sectsubscribe_email: email,
				  action: 'ideabuz_subscribe_ajax'
				},
				success: function( data ){
				  $(".newsletter").append(data);
				  $(".newsletter .alert").delay(3000).fadeOut();
				}
			});
          
          return false;
		  
        });

		
	} );
})(jQuery);