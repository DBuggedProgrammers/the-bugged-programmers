class CustomHeader extends HTMLElement {
    connectedCallback() {
      this.innerHTML = `
      <header class="header fixed-top">
    <!-- Header Style One Begin -->
    <div class="fixed-top header-main style--one">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-sm-4 col-8">
                    <!-- Logo Begin -->
                    <div class="logo">
                        <a href="../index.html">
							<img src="../wp-content/uploads/2019/07/logo-1.png" class="default-logo" alt="logo" />
							<img src="../wp-content/uploads/2019/06/sticky_logo.png" class="sticky-logo" alt="logo" />
						</a>
					</div>
                    <!-- Logo End -->
                </div>

                <div class="col-lg-9 col-sm-8 col-4">
                    <!-- Main Menu Begin -->
                    <div class="main-menu d-flex align-items-center justify-content-end">
                        <ul id="menu-primary-menu" class="nav align-items-center">
							<li id="menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-parent menu-item-home menu-item-has-children menu-item-43">
								<a href="../index.html">Home</a>
							</li>

							<li id="menu-item-59" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-59 about-highlight">
								<a href="../about-us/index.html">About Us</a>
							</li>

							<li id="menu-item-467" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-467 services-highlight">
							<a href="../services/index.html">Services</a>
								
							</li>

							<li id="menu-item-58" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-58 portfolio-highlight">
							<a href="../projects/index.html">Portfolio</a>
							<ul class="sub-menu">
								<li id="menu-item-477" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-477">
									<a href="../projects/websiteproject.html">Website Development</a>
								</li>

								<li id="menu-item-478" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-478">
									<a href="../projects/appproject.html">Mobile App Development</a>
								</li>

								<!--<li id="menu-item-945" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-945">-->
									<!--<a href="../projects/gameproject.html">Game Development</a>-->
								<!--</li>-->

								<li id="menu-item-945" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-945">
									<a href="../projects/UX.html">UI/UX Designing</a>
								</li>

								<!-- <li id="menu-item-945" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-945">
									<a href="../projects/Softwarewebsite.html">Software Development</a>
								</li> -->

								<li id="menu-item-945" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-945">
									<a href="../projects/digitalmarketingproject.html">Digital Media Marketing</a>
								</li>
							</ul>
							</li>

							

							<!--<li id="menu-item-61" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-61">
								<a href="../blog/index.html">Blog</a>
							</li>-->

							<li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-41 current_page_item menu-item-48 contact-highlight">
								<a href="../contact/index.html" aria-current="page">Contact</a>
							</li>
						</ul>

						<!-- Offcanvas Holder Trigger -->
						<span class="offcanvas-trigger text-right d-none d-lg-block">
							<span></span>
							<span></span>
							<span></span>
						</span>
                                                <!-- Offcanvas Trigger End -->
                    </div>
                    <!-- Main Menu ENd -->
                </div>
            </div>
        </div>
    </div>
    <!-- Header Style One End -->
</header>

<div class="offcanvas-overlay fixed-top w-100 h-100"></div>
<div class="offcanvas-wrapper bg-white fixed-top h-100">
	<!-- Offcanvas Close Button Begin -->
	<div class="offcanvas-close position-absolute">
		<img src="../wp-content/themes/ideabuz/assets/img/icons/close.svg" alt="close"  class="svg" />	</div>
	<!-- Offcanvas Close Button End -->

	<!-- Offcanvas Content Begin -->
	<div class="offcanvas-content">
		<div class="widget widget_about">
			<div class="widget-logo">
				<img src="../wp-content/uploads/2019/07/logo-1.png" alt="logo 1"  />
			</div>
				<div class="about-content">
					<p>We belive in Creativity Over Convention</p>
				</div>
		</div>

        <div class="widget widget_flicker">
            <div class="widget-title">
                <!--<h4>Latest Work</h4>-->
            </div>
            <img title='Mumbai' src='../photos/Mumbai.gif' alt='Mumbai' />
            <!--<ul class="d-flex flex-wrap">
        <li>
            <a href="about-us/index.html"><img title='rj_bg' src='../../../farm1.staticflickr.com/709/23437895515_c006333860_q.jpg' alt='rj_bg' /></a>
        </li>
        <li>
            <a href="about-us/index.html"><img title='rj_bg' src='../../../farm1.staticflickr.com/709/23437895515_c006333860_q.jpg' alt='rj_bg' /></a>
        </li>
        <li>
            <a href="about-us/index.html"><img title='rj_bg' src='../../../farm1.staticflickr.com/709/23437895515_c006333860_q.jpg' alt='rj_bg' /></a>
        </li>
    </ul>-->
        </div>

				<div class="widget widget_contact_info">    <!-- About Widget Start -->
				<!-- Widget Logo Begin -->
					<div class="widget-title"><h4>Get In Touch</h4></div>		<!-- Widget Content Begin -->
					<div class="info-content">
						<div class="single-info">
							<span>Office Location:</span>
							<p>702, Jineshwar Dham, </br>Rokadia Cross Lane, </br>Borivali (W), Mumbai - 400092.</p>
						</div>

						<div class="single-info">
							<span>Business Phone:</span>
							<p>
								<a href="tel:+91 9869316180" >+91 9869316180</a>
								<a href="tel:+91 8082641147" >+91 8082641147</a>
							</p>
						</div>

						<div class="single-info">
							<span>Support mail:</span>
							<p>
								<a href="mailto:d.buggedprogrammers@gmail.com" >d.buggedprogrammers@gmail.com</a>
							</p>
						</div>
					</div>
				<!-- Widget Content End -->
				</div>

				<div class="offcanvas-btn">
					<a href="index.html" class="btn">
						<span>Contact Us</span>
					</a>
				</div>
			<!-- About Widget End -->
			</div>
		<!-- Offcanvas Content End -->
		</div>
<!-- Offcanvas End -->
      `;
    }
  }
      
  customElements.define('custom-header', CustomHeader);

  class CustomFooter extends HTMLElement {
    connectedCallback() {
      this.innerHTML = `
      <!-- Footer Top Begin -->
		<footer class="footer section-pattern footer-bg">
		    <div class="footer-top pt-60">
		        <div class="container border-bottom">
		            <div class="row">
						<div class="col-lg-3 col-sm-6">
		                    <div class="widget widget_nav_menu">
								<div class="widget-title">
									<h4>Company</h4>
								</div>
								<div class="menu-quick-link-container">
                                    <ul id="menu-quick-link" class="menu">
                                        <li><a href="../index.html">Home</a></li>
                                        <li><a href="../about-us/index.html">About Us</a></li>
                                        <li><a href="../services/index.html">Services</a></li>
                                        <li><a href="../projects/index.html">Portfolio</a></li>
                                        <!--<li><a href="../blog/index.html">Blog</a></li>-->
                                        <li><a href="../contact/index.html">Contact</a></li>
                                    </ul>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
		                    <div class="widget widget_nav_menu">
								<div class="widget-title">
									<h4>Services</h4>
								</div>
								<div class="menu-quick-link-container">
                                    <ul id="menu-quick-link" class="menu">
                                        <li><a href="../services/index.html">Website Development</a></li>
                                        <li><a href="../services/index.html">Mobile App Development</a></li>
                                        <!--<li><a href="../projects/gameproject.html">Games</a></li>-->
                                        <li><a href="../services/index.html">UI/UX Designing</a></li>
                                        <!-- <li><a href="../services/index.html">Software Development</a></li> -->
                                        <li><a href="../services/index.html">Digital Media Marketing</a></li>
                                    </ul>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
		                    <div class="widget widget_nav_menu">
								<div class="widget-title">
                                    <h4>Quick Links</h4>
								</div>
								<div class="menu-quick-link-container">
                                    <ul id="menu-quick-link" class="menu">
                                        <!-- <li><a href="blog/index.html">Blog</a></li>
                                        <li><a href="contact/index.html">Contact</a></li>
                                        <li><a href="about-us/index.html">About Us</a></li>
                                        <li><a href="projects/index.html">Projects</a></li> -->
                                        <li><a href="../contact/index.html">Career</a></li>
                                        <!--<li><a href="#">Privacy Policy</a></li>-->
                                        <!--<li><a href="#">Terms &#038; Conditions</a></li>-->
                                    </ul>
								</div>
							</div>
						</div>


						<div class="col-lg-3 col-sm-6">
		                    <div class="widget widget_nav_menu">
								<div class="widget-title">
									<h4>Contact</h4>
								</div>
								<div class="info-content">
									<div class="single-info">
										Office Location:
									</div>
									<p>
										<a href="#" >702, Jineshwar Dham, Rokadia Cross Lane, Borivali (W), </br>Mumbai - 400092.</a>
									</p>

									<div class="single-info">
										<span>Business Phone:</span>
									</div>
									<p>
								<a href="tel:+91 9869316180" >+91 9869316180</a></br>
								<a href="tel:+91 8082641147" >+91 8082641147</a>
									</p>

									<div class="single-info">
										<span>Support mail:</span>
									</div>
									<p>
										<a href="mailto:info@dbuggedprogrammers.com" >info@dbuggedprogrammers.com</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>

			<!-- Footer Top End -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
					<div class="col-10">
					<div class="row">
					<div class="copyright-text text-left">
						<span>
							<a href="#">D'Bugged Programmers</a> © Copyright 2020. All rights reserved.
						</span>
						
					</div>
				</div>
				<div class="row">
						<div class="copyright-text text-left">
							<span>
								<img src="https://img.icons8.com/ios-filled/40/000000/gateway-of-india.png"/> 
							</span>
							<span>Made with Love in India</span> <img src="https://img.icons8.com/color/20/000000/india.png"/>
							
						</div>
					</div>
				</div>

						<div class = "col-2">
							<!-- Widget Social Icon Begin -->
							<div class="widget widget_social_icon">
								<ul class="social_icon_list list-inline">
									<li><a href="#"><i class="fa fa-facebook" style="font-size:18px" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" style="font-size:18px" aria-hidden="true"></i></a></li>
									<li><a href="http://www.linkedin.com/in/dbugged-programmers/"><i class="fa fa-linkedin" style="font-size:18px" aria-hidden="true"></i></a></li>
									<li><a href="https://www.instagram.com/dbugged_programmers/"><i class="fa fa-instagram" style="font-size:18px" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<!-- Widget Social Icon End -->
						</div>
					</div>
				</div>
			</div>
		</footer>
	<!-- End of footer -->
      `;
    }
  }
      
  customElements.define('custom-footer', CustomFooter);